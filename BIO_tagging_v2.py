import re

def readfile():
    fcon = open('./training_data/training_data/beth/concept/record-13.con')
    ftxt = open('./training_data/training_data/beth/txt/record-13.txt')
    fconlines = fcon.readlines()
    fcon_dict = {}
    for line in fconlines:
        line_numbers = re.findall(r'(\d+):\d+', line)
        word_numbers = re.findall(r'\d+:(\d+)', line)
        num_of_words = int(word_numbers[1]) - int(word_numbers[0]) + 1
        words = re.findall(r'"([^"]*)"', line)
        print(line_numbers)
        print(word_numbers)
        print(words)
        for i in line_numbers:
            try:
                for j in range(num_of_words):
                    if j == 0:
                        fcon_dict[int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'B-'+words[1]]
                    else:
                        fcon_dict[int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'I-'+words[1]]
            except:
                fcon_dict[int(i)] = {}
                for j in range(num_of_words):
                    if j == 0:
                        fcon_dict[int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'B-'+words[1]]
                    else:
                        fcon_dict[int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'I-'+words[1]]   

    line_count = 0
    total_words_list = []
    total_tags_list = []
    sentence_num_list = []
    for line in ftxt:
        line_count+=1
        txt_words = line.split()
        if line_count in fcon_dict.keys():
            for i in range(len(txt_words)):
                if i in fcon_dict[line_count].keys():
                    total_words_list.append(txt_words[i])
                    total_tags_list.append(fcon_dict[line_count][i][1])
                    sentence_num_list.append(line_count)
                else:
                    total_words_list.append(txt_words[i])
                    total_tags_list.append('O')
                    sentence_num_list.append(line_count)
        else:
            for i in range(len(txt_words)):
                total_words_list.append(txt_words[i])
                total_tags_list.append('O')
                sentence_num_list.append(line_count)
    print('\nfcon_dict\n')
    print(fcon_dict)
    print('\ntotal_words_list\n')
    print(total_words_list)
    print('\ntotal_tags_list\n')
    print(total_tags_list)
    print('\nsentence_num_list\n')
    print(sentence_num_list)

readfile()