import re

##Reading a file##
def readfile():
    fcon = open('./training_data/training_data/beth/concept/record-13.con')
    ftxt = open('./training_data/training_data/beth/txt/record-13.txt')
    fconlines = fcon.readlines()
    count = 0
    entity_word_list = []
    tag_list = []
    dict_line_words = {}
    for line in fconlines:
        line_numbers = re.findall(r'(\d+):\d+', line)
        word_numbers = re.findall(r'\d+:(\d+)', line)
        for i in range(len(line_numbers)):
            try:
                dict_line_words[int(line_numbers[i])].append(int(word_numbers[i]))
            except:
                dict_line_words[int(line_numbers[i])] = [int(word_numbers[i])]
        wordsNtags = re.findall(r'"([^"]*)"', line)
        words = re.split('\s', wordsNtags[0])
        word_num = 0
        for i in words:
            entity_word_list.append(i.lower())
            if word_num == 0:
                tag_list.append('B-'+wordsNtags[1])
            else:
                tag_list.append('I-'+wordsNtags[1])
            word_num+=1
    print('\nentity_word_list:\n')
    print(entity_word_list)
    print('\ntag_list:\n')
    print(tag_list)
    print('\nLine and word numbers:\n')
    print(dict_line_words)
    out_words_list = []
    out_tag_list = []
    count = 0
    total_words_list = []
    total_tags = []
    for line in ftxt:
        count+=1
        txt_words = line.split()
        # count2 = 0
        for i in range(len(txt_words)):
            if count in dict_line_words.keys():
                if i>=dict_line_words[count][0] and i<=dict_line_words[count][1]:
                    total_words_list.append(txt_words[i].lower())
                    total_tags.append(tag_list[entity_word_list.index(txt_words[i].lower())])
                    # count2+=1
                else:
                    total_words_list.append(txt_words[i].lower())
                    total_tags.append('O')
            else:
                total_words_list.append(txt_words[i].lower())
                total_tags.append('O')
    print('\ntotal_words_list:\n')
    print(total_words_list)
    print('\ntotal_tags:\n')
    print(total_tags)
readfile()