import re
import pandas as pd
import numpy as np
import itertools
import nltk
import os
import glob


class BIO_tagging(object):
    def __init__(self):
        self.line_count = 0
        self.total_words_list = []
        self.total_tags_list = []
        self.sentence_num_list = []


    def readfile(self, fcon_file, ftxt_file, line_count, total_words_list, total_tags_list, sentence_num_list):
        fcon = open(fcon_file)
        ftxt = open(ftxt_file)
        fconlines = fcon.readlines()
        fcon_dict = {}
        for line in fconlines:
            line_numbers = re.findall(r'(\d+):\d+', line)
            word_numbers = re.findall(r'\d+:(\d+)', line)
            num_of_words = int(word_numbers[1]) - int(word_numbers[0]) + 1
            words = re.findall(r'"([^"]*)"', line)

            # print(line_numbers)
            # print(word_numbers)
            # print(words)

            for i in line_numbers:
                try:
                    for j in range(num_of_words):
                        if j == 0:
                            fcon_dict[line_count+int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'B-'+words[1]]
                        else:
                            fcon_dict[line_count+int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'I-'+words[1]]
                except:
                    fcon_dict[line_count+int(i)] = {}
                    for j in range(num_of_words):
                        if j == 0:
                            fcon_dict[line_count+int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'B-'+words[1]]
                        else:
                            fcon_dict[line_count+int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'I-'+words[1]]   

        for line in ftxt:
            line_count+=1
            txt_words = line.split()
            if line_count in fcon_dict.keys():
                for i in range(len(txt_words)):
                    if i in fcon_dict[line_count].keys():
                        total_words_list.append(txt_words[i])
                        total_tags_list.append(fcon_dict[line_count][i][1])
                        sentence_num_list.append(line_count)
                    else:
                        total_words_list.append(txt_words[i])
                        total_tags_list.append('O')
                        sentence_num_list.append(line_count)
            else:
                for i in range(len(txt_words)):
                    total_words_list.append(txt_words[i])
                    total_tags_list.append('O')
                    sentence_num_list.append(line_count)
        self.line_count = line_count
        self.total_words_list = total_words_list
        self.total_tags_list = total_tags_list
        self.sentence_num_list = sentence_num_list

bio_tagging = BIO_tagging()


con_path = 'D:/Data_Downloads/BMI598/Project/test_data/reference_standard_for_test_data/concept'
txt_path = 'D:/Data_Downloads/BMI598/Project/test_data/reference_standard_for_test_data/txt'
con_files = [f for f in glob.glob(con_path + "**/*.con", recursive=True)]
txt_files = [f for f in glob.glob(txt_path + "**/*.txt", recursive=True)]
print(con_files[26])
# print(len(con_files))
print(txt_files[26])
# print(len(txt_files))


for i in range(27):
    bio_tagging.readfile(con_files[i], txt_files[i], bio_tagging.line_count, bio_tagging.total_words_list, bio_tagging.total_tags_list, bio_tagging.sentence_num_list)

# print(bio_tagging.sentence_num_list)
# print(bio_tagging.total_words_list)
# print(bio_tagging.total_tags_list)

words_pos_tag = nltk.pos_tag(bio_tagging.total_words_list)
tokens, pos_tags = zip(*words_pos_tag)
#Converting the list to np to stack on top of each other
complete_list = np.column_stack((bio_tagging.sentence_num_list, bio_tagging.total_words_list, pos_tags, bio_tagging.total_tags_list))
complete_list = (complete_list.tolist())
#Converting the list to dataframe
df = pd.DataFrame(data = complete_list)
df.columns =['Sentence #', 'Word', 'POS', 'Tag']
# print(df)
df.to_csv('test_data.csv')