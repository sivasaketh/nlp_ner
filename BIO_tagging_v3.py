import re
import pandas as pd
import numpy as np
import itertools
import nltk

def readfile():
    fcon = open('./training_data/training_data/beth/concept/record-13.con')
    ftxt = open('./training_data/training_data/beth/txt/record-13.txt')
    fconlines = fcon.readlines()
    fcon_dict = {}
    for line in fconlines:
        line_numbers = re.findall(r'(\d+):\d+', line)
        word_numbers = re.findall(r'\d+:(\d+)', line)
        num_of_words = int(word_numbers[1]) - int(word_numbers[0]) + 1
        words = re.findall(r'"([^"]*)"', line)

        # print(line_numbers)
        # print(word_numbers)
        # print(words)

        for i in line_numbers:
            try:
                for j in range(num_of_words):
                    if j == 0:
                        fcon_dict[int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'B-'+words[1]]
                    else:
                        fcon_dict[int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'I-'+words[1]]
            except:
                fcon_dict[int(i)] = {}
                for j in range(num_of_words):
                    if j == 0:
                        fcon_dict[int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'B-'+words[1]]
                    else:
                        fcon_dict[int(i)][int(word_numbers[0]) + j] = [words[0].split()[j], 'I-'+words[1]]   

    line_count = 0
    total_words_list = []
    total_tags_list = []
    sentence_num_list = []
    for line in ftxt:
        line_count+=1
        txt_words = line.split()
        if line_count in fcon_dict.keys():
            for i in range(len(txt_words)):
                if i in fcon_dict[line_count].keys():
                    total_words_list.append(txt_words[i])
                    total_tags_list.append(fcon_dict[line_count][i][1])
                    sentence_num_list.append(line_count)
                else:
                    total_words_list.append(txt_words[i])
                    total_tags_list.append('O')
                    sentence_num_list.append(line_count)
        else:
            for i in range(len(txt_words)):
                total_words_list.append(txt_words[i])
                total_tags_list.append('O')
                sentence_num_list.append(line_count)

    # fcon_list = ([(k,v) for k,v in fcon_dict.items()])
    words_pos_tag = nltk.pos_tag(total_words_list)
    tokens, pos_tags = zip(*words_pos_tag)

    # print(*fcon_list, sep = "\n")
    # print('\ntotal_words_list\n')
    # print(len(total_words_list))
    # print('\ntotal_tags_list\n')
    # print(len(total_tags_list))

    #Converting the list to np to stack on top of each other
    complete_list = np.column_stack((sentence_num_list, total_words_list, pos_tags, total_tags_list))
    complete_list = (complete_list.tolist())
    #Converting the list to dataframe
    df = pd.DataFrame(data = complete_list)
    # df = df.transpose(data=)
    df.columns =['Sentence #', 'Word', 'POS', 'Tag']

    return df

df_words_tags = readfile()
print(df_words_tags)

# class SentenceGetter(object):
    
#     def __init__(self, data):
#         self.n_sent = 1
#         self.data = data
#         self.empty = False
#         agg_func = lambda s: [(w, p, t) for w, p, t in zip(s["Word"].values.tolist(),
#                                                            s["POS"].values.tolist(),
#                                                            s["Tag"].values.tolist())]
#         self.grouped = self.data.groupby("Sentence #").apply(agg_func)
#         self.sentences = [s for s in self.grouped]
#         # print(self.sentences)
    
#     def get_next(self):
#         try:
#             s = self.grouped["Sentence: {}".format(self.n_sent)]
#             self.n_sent += 1
#             return s
#         except:
#             return None

# getter = SentenceGetter(df_words_tags)
# sentences = getter.sentences

# labels = [[s[2] for s in sent] for sent in sentences]
# sentences = [" ".join([s[0] for s in sent]) for sent in sentences]

# print(labels)

